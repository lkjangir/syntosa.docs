# Syntosa.Core.ObjectModel.EdgeElementLabel
** need class/table description **
DataTable: EDGE_ELEMENT_LABEL_XREF

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|** need property description **|Int32|True|True|
|UId|** need property description **|Guid|True|True|
|LeftRecordType|** need property description **|SyntosaRecordType|True|False|
|LeftUId|** need property description **|Guid|True|True|
|LabelUId|** need property description **|Guid|True|True|
|LabelName|** need property description **|String|True|False|
|IsLabelActive|** need property description **|Boolean|True|False|
|RightRecordType|** need property description **|SyntosaRecordType|True|False|
|RightUId|** need property description **|Guid|True|True|
|ElementUId|** need property description **|Guid|True|True|
|ElementName|** need property description **|String|True|False|
|IsElementActive|** need property description **|Boolean|True|False|
|ParentElementUId|** need property description **|Guid|True|False|
|IsLeftUIdSource|** need property description **|Boolean|True|True|
|TypeItemUId|** need property description **|Guid|True|True|
|TypeItemName|** need property description **|String|True|True|
|IsTypeItemActive|** need property description **|Boolean|True|True|
|Name|** need property description **|String|True|True|
|ModifiedBy|The last user to modify the item.|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|EDGE_ELEMENT_LABEL_XREF_PK|OneWayFromSourceRequired|Pk|edgeElementLabelId|SqlDbType.Int|False|
|UId|EDGE_ELEMENT_LABEL_XREF_UID|TwoWayRequired|UId|edgeElementLabelUId|SqlDbType.UniqueIdentifier|False|
|LeftRecordType|None|None|Unknown|-|-|N/A|
|LeftUId|None|None|Unknown|-|-|N/A|
|LabelUId|ELEMENT_LABEL_UID|TwoWayRequired|Unknown|labelUId|-|False|
|LabelName|LABEL_NAME|OneWayFromSourceRequired|Unknown|labelName|-|False|
|IsLabelActive|ISACTIVE_ELEMENT_LABEL|OneWayFromSourceRequired|Unknown|isLabelActive|-|False|
|RightRecordType|None|None|Unknown|-|-|N/A|
|RightUId|None|None|Unknown|-|-|N/A|
|ElementUId|ELEMENT_UID|TwoWayRequired|Unknown|elementUId|-|False|
|ElementName|ELEMENT_NAME|OneWayFromSourceRequired|Unknown|elementName|-|False|
|IsElementActive|ISACTIVE_ELEMENT|OneWayFromSourceRequired|Unknown|isElementActive|-|False|
|ParentElementUId|ELEMENT_UID_PARENT|OneWayFromSourceRequired|Unknown|parentElementUId|-|False|
|IsLeftUIdSource|None|None|Unknown|-|-|N/A|
|TypeItemUId|-|None|Unknown|typeItemUId|-|N/A|
|TypeItemName|-|None|Unknown|typeItemName|-|N/A|
|IsTypeItemActive|-|None|Unknown|isTypeItemActive|-|N/A|
|Name|-|None|Unknown|name|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
