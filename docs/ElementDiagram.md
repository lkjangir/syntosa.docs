# Syntosa.Core.ObjectModel.ElementDiagram
** need class/table description **
DataTable: ELEMENT_DIAGRAM

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|The primary key of this Diagram.|Int32|True|True|
|UId|The unique identifier of this Diagram.|Guid|True|True|
|ElementUId|** need property description **|Guid|True|True|
|Diagram|** need property description **|String|True|True|
|ElementName|** need property description **|String|True|False|
|IsElementActive|** need property description **|Boolean|True|False|
|ParentUId|** need property description **|Guid|True|True|
|Children|** need property description **|List`1|True|True|
|Syntosa.Core.ObjectModel.ISyntosaHierRecord.Children|** need property description **|IList|True|False|
|RlsOwner|The row-level security Owner UId.|Guid|True|True|
|RlsMask|The row-level security bitmask for this Diagram.|Byte[]|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|Name|** need property description **|String|True|True|
|ModifiedBy|The last user to modify the item.|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|ELEMENT_DIAGRAM_PK|OneWayFromSourceRequired|Pk|elementDiagramId|SqlDbType.Int|False|
|UId|ELEMENT_DIAGRAM_UID|TwoWayRequired|UId|elementDiagramUId|SqlDbType.UniqueIdentifier|False|
|ElementUId|ELEMENT_UID|TwoWayRequired|Unknown|elementUId|-|False|
|Diagram|DIAGRAM|TwoWayRequired|Unknown|diagram|-|False|
|ElementName|ELEMENT_NAME|OneWayFromSourceRequired|Unknown|elementName|-|False|
|IsElementActive|ISACTIVE_ELEMENT|OneWayFromSourceRequired|Unknown|isElementActive|-|False|
|ParentUId|ELEMENT_DIAGRAM_UID_PARENT|TwoWayRequired|ParentUId|parentUId|-|False|
|Children|None|None|Unknown|-|-|N/A|
|Syntosa.Core.ObjectModel.ISyntosaHierRecord.Children|None|None|Unknown|-|-|N/A|
|RlsOwner|RLS_OWNER|TwoWayRequired|Unknown|rlsOwner|-|True|
|RlsMask|RLS_MASK|TwoWayRequired|Unknown|rlsMask|-|True|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|Name|None|None|Unknown|-|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
