# Syntosa.Core.ObjectModel.EdgeElementGlobalPropertyProperty
** need class/table description **
DataTable: EDGE_ELEMENT_GLOBALPROPERTY_XREF_PROPERTY

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|** need property description **|Int32|True|True|
|UId|** need property description **|Guid|True|True|
|EdgeType|** need property description **|SyntosaRecordType|True|False|
|EdgeUId|** need property description **|Guid|True|True|
|IsActiveTypeEdge|** need property description **|Boolean|True|False|
|ElementGlobalPropertyUId|** need property description **|Guid|True|False|
|ElementUId|** need property description **|Guid|True|False|
|ElementUIdDriver|** need property description **|Boolean|True|False|
|IsActiveElement|** need property description **|Boolean|True|False|
|ElementName|** need property description **|String|True|False|
|TypeUIdElement|** need property description **|Guid|True|False|
|IsActiveElementType|** need property description **|Boolean|True|False|
|TypeNameElement|** need property description **|String|True|False|
|TypeUIdGlobalProperty|** need property description **|Guid|True|False|
|GlobalAttribute|** need property description **|String|True|False|
|TypeNameGlobalProperty|** need property description **|String|True|False|
|ElementNameGlobalProperty|** need property description **|String|True|False|
|IsActiveGlobalPropertyElement|** need property description **|Boolean|True|False|
|TypeUIdGlobalPropertyElement|** need property description **|Guid|True|False|
|Name|** need property description **|String|True|True|
|ModifiedBy|The last user to modify the item.|String|True|True|
|TypeItemUId|** need property description **|Guid|True|True|
|TypeItemName|** need property description **|String|True|True|
|Attribute|** need property description **|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|EDGE_ELEMENT_GLOBALPROPERTY_XREF_PROPERTY_PK|OneWayFromSourceRequired|Pk|edgeElementGlobalPropId|SqlDbType.Int|False|
|UId|EDGE_ELEMENT_GLOBALPROPERTY_XREF_PROPERTY_UID|TwoWayRequired|UId|edgeElementGlobalPropUId|SqlDbType.UniqueIdentifier|False|
|EdgeType|None|None|Unknown|-|-|N/A|
|EdgeUId|EDGE_ELEMENT_GLOBALPROPERTY_XREF_UID|TwoWayRequired|Unknown|edgeUId|-|False|
|IsActiveTypeEdge|ISACTIVE_TYPE_EDGE|OneWayFromSourceRequired|Unknown|isActiveTypeEdge|-|False|
|ElementGlobalPropertyUId|ELEMENT_GLOBALPROPERTY_UID|OneWayFromSourceRequired|Unknown|elementGlobalPropertyUId|-|False|
|ElementUId|ELEMENT_UID|OneWayFromSourceRequired|Unknown|elementUId|-|False|
|ElementUIdDriver|ELEMENT_UID_DRIVER|OneWayFromSourceRequired|Unknown|elementUIdDriver|-|False|
|IsActiveElement|ISACTIVE_ELEMENT|OneWayFromSourceRequired|Unknown|isActiveElement|-|False|
|ElementName|ELEMENT_NAME|OneWayFromSourceRequired|Unknown|elementName|-|False|
|TypeUIdElement|TYPE_UID_ELEMENT|OneWayFromSourceRequired|Unknown|typeUIdElement|-|False|
|IsActiveElementType|ISACTIVE_ELEMENT_TYPE|OneWayFromSourceRequired|Unknown|isActiveElementType|-|False|
|TypeNameElement|TYPE_NAME_ELEMENT|OneWayFromSourceRequired|Unknown|typeNameElement|-|False|
|TypeUIdGlobalProperty|TYPE_UID_GLOBALPROPERTY|OneWayFromSourceRequired|Unknown|typeUIdGlobalProperty|-|False|
|GlobalAttribute|GLOBAL_ATTRIBUTE|OneWayFromSourceRequired|Unknown|globalAttribute|-|False|
|TypeNameGlobalProperty|TYPE_NAME_GLOBALPROPERTY|OneWayFromSourceRequired|Unknown|typeNameGlobalProperty|-|False|
|ElementNameGlobalProperty|ELEMENT_NAME_GLOBALPROPERTY|OneWayFromSourceRequired|Unknown|elementNameGlobalProperty|-|False|
|IsActiveGlobalPropertyElement|ISACTIVE_GLOBALPROPERTY_ELEMENT|OneWayFromSourceRequired|Unknown|isActiveGlobalPropertyElement|-|False|
|TypeUIdGlobalPropertyElement|TYPE_UID_GLOBALPROPERTY_ELEMENT|OneWayFromSourceRequired|Unknown|typeUIdGlobalPropertyElement|-|False|
|Name|-|None|Unknown|name|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|TypeItemUId|TYPE_UID_EDGE_PROPERTY|TwoWayRequired|Unknown|typeItemUId|-|False|
|TypeItemName|TYPE_NAME_EDGE_PROPERTY|OneWayFromSourceRequired|Name|typeItemName|-|False|
|Attribute|EDGE_PROPERTY_ATTRIBUTE|TwoWayRequired|Unknown|attribute|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
