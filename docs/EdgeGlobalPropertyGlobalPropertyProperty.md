# Syntosa.Core.ObjectModel.EdgeGlobalPropertyGlobalPropertyProperty
** need class/table description **
DataTable: EDGE_ELEMENT_GLOBALPROPERTY_GLOBALPROPERTY_XREF_PROPERTY

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|** need property description **|Int32|True|True|
|UId|** need property description **|Guid|True|True|
|EdgeType|** need property description **|SyntosaRecordType|True|False|
|EdgeUId|** need property description **|Guid|True|True|
|IsActiveTypeEdgeProperty|** need property description **|Boolean|True|False|
|TypeUIdEdge|** need property description **|Guid|True|False|
|TypeNameEdge|** need property description **|String|True|False|
|TypeUIdGlobalPropertySource|** need property description **|Guid|True|False|
|GlobalAttributeSource|** need property description **|String|True|False|
|TypeNameEdgeSource|** need property description **|String|True|False|
|TypeUIdGlobalPropertyTarget|** need property description **|Guid|True|False|
|GlobalAttributeTarget|** need property description **|String|True|False|
|TypeNameEdgeTarget|** need property description **|String|True|False|
|ElementNameSource|** need property description **|String|True|False|
|IsActiveElementSource|** need property description **|Boolean|True|False|
|TypeUIdElementSource|** need property description **|Guid|True|False|
|ElementNameTarget|** need property description **|String|True|False|
|IsActiveElementTarget|** need property description **|Boolean|True|False|
|TypeUIdElementTarget|** need property description **|Guid|True|False|
|Name|** need property description **|String|True|True|
|ModifiedBy|The last user to modify the item.|String|True|True|
|TypeItemUId|** need property description **|Guid|True|True|
|TypeItemName|** need property description **|String|True|True|
|Attribute|** need property description **|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|EDGE_ELEMENT_GLOBALPROPERTY_GLOBALPROPERTY_XREF_PROPERTY_PK|OneWayFromSourceRequired|Pk|edgeGlobalGlobalPropId|SqlDbType.Int|False|
|UId|EDGE_ELEMENT_GLOBALPROPERTY_GLOBALPROPERTY_XREF_PROPERTY_UID|TwoWayRequired|UId|edgeGlobalGlobalPropUId|SqlDbType.UniqueIdentifier|False|
|EdgeType|None|None|Unknown|-|-|N/A|
|EdgeUId|EDGE_ELEMENT_GLOBALPROPERTY_GLOBALPROPERTY_XREF_UID|TwoWayRequired|Unknown|edgeUId|-|False|
|IsActiveTypeEdgeProperty|ISACTIVE_TYPE_EDGE_PROPERTY|OneWayFromSourceRequired|Unknown|isActiveTypeEdgeProperty|-|False|
|TypeUIdEdge|TYPE_UID_EDGE|OneWayFromSourceRequired|Unknown|typeUIdEdge|-|False|
|TypeNameEdge|TYPE_NAME_EDGE|OneWayFromSourceRequired|Unknown|typeNameEdge|-|False|
|TypeUIdGlobalPropertySource|TYPE_UID_GLOBALPROPERTY_SOURCE|OneWayFromSourceRequired|Unknown|typeUIdGlobalPropertySource|-|False|
|GlobalAttributeSource|GLOBAL_ATTRIBUTE_SOURCE|OneWayFromSourceRequired|Unknown|globalAttributeSource|-|False|
|TypeNameEdgeSource|TYPE_NAME_EDGE_SOURCE|OneWayFromSourceRequired|Unknown|typeNameEdgeSource|-|False|
|TypeUIdGlobalPropertyTarget|TYPE_UID_GLOBALPROPERTY_TARGET|OneWayFromSourceRequired|Unknown|typeUIdGlobalPropertyTarget|-|False|
|GlobalAttributeTarget|GLOBAL_ATTRIBUTE_TARGET|OneWayFromSourceRequired|Unknown|globalAttributeTarget|-|False|
|TypeNameEdgeTarget|TYPE_NAME_EDGE_TARGET|OneWayFromSourceRequired|Unknown|typeNameEdgeTarget|-|False|
|ElementNameSource|ELEMENT_NAME_SOURCE|OneWayFromSourceRequired|Unknown|elementNameSource|-|False|
|IsActiveElementSource|ISACTIVE_ELEMENT_SOURCE|OneWayFromSourceRequired|Unknown|isActiveElementSource|-|False|
|TypeUIdElementSource|TYPE_UID_ELEMENT_SOURCE|OneWayFromSourceRequired|Unknown|typeUIdElementSource|-|False|
|ElementNameTarget|ELEMENT_NAME_TARGET|OneWayFromSourceRequired|Unknown|elementNameTarget|-|False|
|IsActiveElementTarget|ISACTIVE_ELEMENT_TARGET|OneWayFromSourceRequired|Unknown|isActiveElementTarget|-|False|
|TypeUIdElementTarget|TYPE_UID_ELEMENT_TARGET|OneWayFromSourceRequired|Unknown|typeUIdElementTarget|-|False|
|Name|-|None|Unknown|name|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|TypeItemUId|TYPE_UID_EDGE_PROPERTY|TwoWayRequired|Unknown|typeItemUId|-|False|
|TypeItemName|TYPE_NAME_EDGE_PROPERTY|OneWayFromSourceRequired|Name|typeItemName|-|False|
|Attribute|EDGE_PROPERTY_ATTRIBUTE|TwoWayRequired|Unknown|attribute|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
