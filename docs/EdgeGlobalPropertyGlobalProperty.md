# Syntosa.Core.ObjectModel.EdgeGlobalPropertyGlobalProperty
** need class/table description **
DataTable: EDGE_ELEMENT_GLOBALPROPERTY_GLOBALPROPERTY_XREF

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|** need property description **|Int32|True|True|
|UId|** need property description **|Guid|True|True|
|LeftRecordType|** need property description **|SyntosaRecordType|True|False|
|LeftUId|** need property description **|Guid|True|True|
|SourceGlobalPropertyUId|** need property description **|Guid|True|True|
|SourceGlobalPropertyTypeUId|** need property description **|Guid|True|False|
|SourceGlobalPropertyTypeName|** need property description **|String|True|False|
|SourceGlobalAttribute|** need property description **|String|True|False|
|SourceElementName|** need property description **|String|True|False|
|IsSourceElementActive|** need property description **|Boolean|True|False|
|SourceElementTypeUId|** need property description **|Guid|True|False|
|RightRecordType|** need property description **|SyntosaRecordType|True|False|
|RightUId|** need property description **|Guid|True|True|
|TargetGlobalPropertyUId|** need property description **|Guid|True|True|
|TargetGlobalPropertyTypeUId|** need property description **|Guid|True|False|
|TargetGlobalPropertyTypeName|** need property description **|String|True|False|
|TargetGlobalAttribute|** need property description **|String|True|False|
|TargetElementName|** need property description **|String|True|False|
|IsTargetElementActive|** need property description **|Boolean|True|False|
|TargetElementTypeUId|** need property description **|Guid|True|False|
|IsLeftUIdSource|** need property description **|Boolean|True|True|
|EdgeProperties|** need property description **|List`1|True|True|
|Syntosa.Core.ObjectModel.ISyntosaEdgeProperty.EdgeProperties|** need property description **|IList|True|False|
|UdtGlobalPropertyUId|Internal use only, supports UDT for TVP calls only.|Guid|True|False|
|Name|** need property description **|String|True|True|
|ModifiedBy|The last user to modify the item.|String|True|True|
|TypeItemUId|** need property description **|Guid|True|True|
|TypeItemName|** need property description **|String|True|True|
|IsTypeItemActive|** need property description **|Boolean|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|EDGE_ELEMENT_GLOBALPROPERTY_GLOBALPROPERTY_XREF_PK|OneWayFromSourceRequired|Pk|edgeGlobalGlobalId|SqlDbType.Int|False|
|UId|EDGE_ELEMENT_GLOBALPROPERTY_GLOBALPROPERTY_XREF_UID|TwoWayRequired|UId|edgeGlobalGlobalUId|SqlDbType.UniqueIdentifier|False|
|LeftRecordType|None|None|Unknown|-|-|N/A|
|LeftUId|None|None|Unknown|-|-|N/A|
|SourceGlobalPropertyUId|ELEMENT_GLOBALPROPERTY_UID_SOURCE|TwoWayRequired|Unknown|sourceGlobalPropertyUId|-|False|
|SourceGlobalPropertyTypeUId|TYPE_UID_GLOBALPROPERTY_SOURCE|OneWayFromSourceRequired|Unknown|sourceGlobalPropertyTypeUId|-|False|
|SourceGlobalPropertyTypeName|TYPE_NAME_GLOBALPROPERTY_SOURCE|OneWayFromSourceRequired|Unknown|sourceGlobalPropertyTypeName|-|False|
|SourceGlobalAttribute|GLOBAL_ATTRIBUTE_SOURCE|OneWayFromSourceRequired|Unknown|sourceGlobalAttribute|-|False|
|SourceElementName|ELEMENT_NAME_SOURCE|OneWayFromSourceRequired|Unknown|sourceElementName|-|False|
|IsSourceElementActive|ISACTIVE_ELEMENT_SOURCE|OneWayFromSourceRequired|Unknown|isSourceElementActive|-|False|
|SourceElementTypeUId|TYPE_UID_ELEMENT_SOURCE|OneWayFromSourceRequired|Unknown|sourceElementTypeUId|-|False|
|RightRecordType|None|None|Unknown|-|-|N/A|
|RightUId|None|None|Unknown|-|-|N/A|
|TargetGlobalPropertyUId|ELEMENT_GLOBALPROPERTY_UID_TARGET|TwoWayRequired|Unknown|targetGlobalPropertyUId|-|False|
|TargetGlobalPropertyTypeUId|TYPE_UID_GLOBALPROPERTY_TARGET|OneWayFromSourceRequired|Unknown|targetGlobalPropertyTypeUId|-|False|
|TargetGlobalPropertyTypeName|TYPE_NAME_GLOBALPROPERTY_TARGET|OneWayFromSourceRequired|Unknown|targetGlobalPropertyTypeName|-|False|
|TargetGlobalAttribute|GLOBAL_ATTRIBUTE_TARGET|OneWayFromSourceRequired|Unknown|targetGlobalAttribute|-|False|
|TargetElementName|ELEMENT_NAME_TARGET|OneWayFromSourceRequired|Unknown|targetElementName|-|False|
|IsTargetElementActive|ISACTIVE_ELEMENT_TARGET|OneWayFromSourceRequired|Unknown|isTargetElementActive|-|False|
|TargetElementTypeUId|TYPE_UID_ELEMENT_TARGET|OneWayFromSourceRequired|Unknown|targetElementTypeUId|-|False|
|IsLeftUIdSource|None|None|Unknown|-|-|N/A|
|EdgeProperties|None|None|Unknown|-|-|N/A|
|Syntosa.Core.ObjectModel.ISyntosaEdgeProperty.EdgeProperties|None|None|Unknown|-|-|N/A|
|UdtGlobalPropertyUId|ELEMENT_GLOBALPROPERTY_UID|None|Unknown|udtGlobalPropertyUId|-|N/A|
|Name|-|None|Unknown|name|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|TypeItemUId|TYPE_UID_EDGE|TwoWayRequired|Unknown|typeItemUId|-|False|
|TypeItemName|TYPE_NAME_EDGE|OneWayFromSourceRequired|Name|typeItemName|-|False|
|IsTypeItemActive|ISACTIVE_TYPE_EDGE|OneWayFromSourceRequired|Unknown|isTypeItemActive|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
