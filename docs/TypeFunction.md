# Syntosa.Core.ObjectModel.TypeFunction
** need class/table description **
DataTable: TYPE_FUNCTION

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|The primary key of this TypeFunction.|Int32|True|True|
|UId|The unique identifier of this TypeFunction.|Guid|True|True|
|Name|The name of this TypeFunction.|String|True|True|
|Description|The friendly description of this TypeFunction.|String|True|True|
|IsActive|Indicates 'deleted' status, where IsActive=false is excluded from normal query operations.|Boolean|True|True|
|IsBuiltIn|Indicates items which are integral to system function.  IsBuiltIn=true items cannot be deleted.|Boolean|True|True|
|ModuleUId|Foreign key to Module table; the Module for this TypeFunction.|Guid|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|ModifiedBy|The last user to modify the item.|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|TYPE_FUNCTION_PK|OneWayFromSourceRequired|Pk|typeFunctionId|SqlDbType.Int|False|
|UId|TYPE_FUNCTION_UID|TwoWayRequired|UId|typeFunctionUId|SqlDbType.UniqueIdentifier|False|
|Name|TYPE_FUNCTION_NAME|TwoWayRequired|Name|typeFunctionName|-|False|
|Description|TYPE_FUNCTION_DESC|TwoWayRequired|Unknown|typeFunctionDesc|-|False|
|IsActive|ISACTIVE|TwoWayOptional|Unknown|isActive|-|False|
|IsBuiltIn|ISBUILTIN|TwoWayOptional|Unknown|isBuiltIn|-|False|
|ModuleUId|MODULE_UID|TwoWayRequired|Unknown|moduleUId|-|False|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
