# Syntosa.Core.ObjectModel.EdgeElementElement
** need class/table description **
DataTable: EDGE_ELEMENT_ELEMENT_XREF

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|** need property description **|Int32|True|True|
|UId|** need property description **|Guid|True|True|
|LeftRecordType|** need property description **|SyntosaRecordType|True|False|
|LeftUId|** need property description **|Guid|True|True|
|SourceElementUId|** need property description **|Guid|True|True|
|SourceElementName|** need property description **|String|True|False|
|IsSourceElementActive|** need property description **|Boolean|True|False|
|SourceElementTypeUId|** need property description **|Guid|True|False|
|RightRecordType|** need property description **|SyntosaRecordType|True|False|
|RightUId|** need property description **|Guid|True|True|
|TargetElementUId|** need property description **|Guid|True|True|
|TargetElementName|** need property description **|String|True|False|
|IsTargetElementActive|** need property description **|Boolean|True|False|
|TargetElementTypeUId|** need property description **|Guid|True|False|
|IsLeftUIdSource|** need property description **|Boolean|True|True|
|EdgeProperties|** need property description **|List`1|True|True|
|Syntosa.Core.ObjectModel.ISyntosaEdgeProperty.EdgeProperties|** need property description **|IList|True|False|
|UdtElementUId|Internal use only, supports UDT for TVP calls only.|Guid|True|False|
|Name|** need property description **|String|True|True|
|ModifiedBy|The last user to modify the item.|String|True|True|
|TypeItemUId|** need property description **|Guid|True|True|
|TypeItemName|** need property description **|String|True|True|
|IsTypeItemActive|** need property description **|Boolean|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|EDGE_ELEMENT_ELEMENT_XREF_PK|OneWayFromSourceRequired|Pk|edgeElementElementId|SqlDbType.Int|False|
|UId|EDGE_ELEMENT_ELEMENT_XREF_UID|TwoWayRequired|UId|edgeElementElementUId|SqlDbType.UniqueIdentifier|False|
|LeftRecordType|None|None|Unknown|-|-|N/A|
|LeftUId|None|None|Unknown|-|-|N/A|
|SourceElementUId|ELEMENT_UID_SOURCE|TwoWayRequired|Unknown|sourceElementUId|-|False|
|SourceElementName|ELEMENT_NAME_SOURCE|OneWayFromSourceRequired|Unknown|sourceElementName|-|False|
|IsSourceElementActive|ISACTIVE_ELEMENT_SOURCE|OneWayFromSourceRequired|Unknown|isSourceElementActive|-|False|
|SourceElementTypeUId|TYPE_UID_ELEMENT_SOURCE|OneWayFromSourceRequired|Unknown|sourceElementTypeUId|-|False|
|RightRecordType|None|None|Unknown|-|-|N/A|
|RightUId|None|None|Unknown|-|-|N/A|
|TargetElementUId|ELEMENT_UID_TARGET|TwoWayRequired|Unknown|targetElementUId|-|False|
|TargetElementName|ELEMENT_NAME_TARGET|OneWayFromSourceRequired|Unknown|targetElementName|-|False|
|IsTargetElementActive|ISACTIVE_ELEMENT_TARGET|OneWayFromSourceRequired|Unknown|isTargetElementActive|-|False|
|TargetElementTypeUId|TYPE_UID_ELEMENT_TARGET|OneWayFromSourceRequired|Unknown|targetElementTypeUId|-|False|
|IsLeftUIdSource|None|None|Unknown|-|-|N/A|
|EdgeProperties|None|None|Unknown|-|-|N/A|
|Syntosa.Core.ObjectModel.ISyntosaEdgeProperty.EdgeProperties|None|None|Unknown|-|-|N/A|
|UdtElementUId|ELEMENT_UID|None|Unknown|udtElementUId|-|N/A|
|Name|-|None|Unknown|name|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|TypeItemUId|TYPE_UID_EDGE|TwoWayRequired|Unknown|typeItemUId|-|False|
|TypeItemName|TYPE_NAME_EDGE|OneWayFromSourceRequired|Name|typeItemName|-|False|
|IsTypeItemActive|ISACTIVE_TYPE_EDGE|OneWayFromSourceRequired|Unknown|isTypeItemActive|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
