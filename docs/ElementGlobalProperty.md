# Syntosa.Core.ObjectModel.ElementGlobalProperty
** need class/table description **
DataTable: ELEMENT_GLOBALPROPERTY

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|The primary key of this GlobalProperty.|Int32|True|True|
|UId|The unique identifier of this GlobalProperty.|Guid|True|True|
|Name|Alias for TypeName field; gets/sets TypeName.|String|True|True|
|ElementUId|Foreign key to Element table.  The Element which owns this GlobalProperty.|Guid|True|True|
|ElementName|** need property description **|String|True|False|
|IsElementActive|** need property description **|Boolean|True|False|
|TypeItemUId|Foreign key to Type table; the Type of this GlobalProperty.|Guid|True|True|
|TypeItemName|The name of thie Type for this GlobalProperty.|String|True|False|
|NameAsKey|The name of thie Type for this GlobalProperty, minus spaces and special characters.|String|True|False|
|IsAutoCollect|Indicates if element data is collected though manual input (AutoCollect=false) or automated processes (AutoCollect=true).|Boolean|True|True|
|ModuleUIdAutoCollect|Foreign key to Module table.  Indicates 'owner' of AutoCollect data; default to Syntosa if IsAutoCollect=false to maintain queryability of data.|Guid|True|True|
|ModuleNameAutoCollect|** need property description **|String|True|False|
|ModuleParentUId|** need property description **|Guid|True|False|
|IsModuleActive|** need property description **|Boolean|True|False|
|SortOrder|Used to manually specify item's position in a list; overrides default alpha sort order.|Int32|True|True|
|Attribute|Reflects attribute value at the relationship point.|String|True|True|
|RlsOwner|The row-level security Owner UId.|Guid|True|True|
|RlsMask|The row-level security bitmask for this Element.|Byte[]|True|True|
|GlobalPropertyEdges|** need property description **|List`1|True|True|
|PrivatePropertyEdges|** need property description **|List`1|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|ModifiedBy|The last user to modify the item.|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|ELEMENT_GLOBALPROPERTY_PK|OneWayFromSourceRequired|Pk|globalPropertyId|SqlDbType.Int|False|
|UId|ELEMENT_GLOBALPROPERTY_UID|TwoWayRequired|UId|globalPropertyUId|SqlDbType.UniqueIdentifier|False|
|Name|-|None|Unknown|name|-|N/A|
|ElementUId|ELEMENT_UID|TwoWayRequired|Unknown|elementUId|-|False|
|ElementName|ELEMENT_NAME|OneWayFromSourceRequired|Unknown|elementName|-|False|
|IsElementActive|ISACTIVE_ELEMENT|OneWayFromSourceRequired|Unknown|isElementActive|-|False|
|TypeItemUId|TYPE_UID|TwoWayRequired|Unknown|typeItemUId|-|False|
|TypeItemName|TYPE_NAME_GLOBALPROPERTY|OneWayFromSourceRequired|Unknown|globalPropertyName|-|False|
|NameAsKey|None|None|Unknown|-|-|N/A|
|IsAutoCollect|ISAUTOCOLLECT|TwoWayRequired|Unknown|isAutoCollect|-|False|
|ModuleUIdAutoCollect|MODULE_UID_AUTOCOLLECT|TwoWayRequired|Unknown|moduleUIdAutoCollect|-|False|
|ModuleNameAutoCollect|AUTOCOLLECT_MODULE_NAME|OneWayFromSourceRequired|Unknown|moduleNameAutoCollect|-|False|
|ModuleParentUId|MODULE_UID_PARENT|OneWayFromSourceRequired|Unknown|moduleParentUId|-|False|
|IsModuleActive|ISACTIVE_MODULE|OneWayFromSourceRequired|Unknown|isModuleActive|-|False|
|SortOrder|UISORT_ORDER|TwoWayRequired|Unknown|sortOrder|-|False|
|Attribute|GLOBAL_ATTRIBUTE|TwoWayRequired|Unknown|attribute|-|False|
|RlsOwner|RLS_OWNER|TwoWayRequired|Unknown|rlsOwner|-|True|
|RlsMask|RLS_MASK|TwoWayRequired|Unknown|rlsMask|-|True|
|GlobalPropertyEdges|None|None|Unknown|-|-|N/A|
|PrivatePropertyEdges|None|None|Unknown|-|-|N/A|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
