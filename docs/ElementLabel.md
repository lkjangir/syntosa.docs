# Syntosa.Core.ObjectModel.ElementLabel
** need class/table description **
DataTable: ELEMENT_LABEL

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|The primary key of this Element.|Int32|True|True|
|UId|The unique identifier of this Element.|Guid|True|True|
|Name|The name of this Element.|String|True|True|
|Description|The friendly description of this Element.|String|True|True|
|IsActive|Indicates 'deleted' status, where IsActive=false is excluded from normal query operations.|Boolean|True|True|
|IsBuiltIn|Indicates items which are integral to system function.  IsBuiltIn=true items cannot be deleted.|Boolean|True|True|
|IsGlobalEdit|*** ?? ***|Boolean|True|True|
|IsPrivate|Indicates if this label is private to the creator.|Boolean|True|True|
|DomainUId|Foreign key to Domain table, represents the owning Domain.|Guid|True|True|
|DomainName|** need property description **|String|True|False|
|IsDomainActive|** need property description **|Boolean|True|False|
|RlsOwner|The row-level security Owner UId.|Guid|True|True|
|RlsMask|The row-level security bitmask for this Element.|Byte[]|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|ModifiedBy|The last user to modify the item.|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|ELEMENT_LABEL_PK|OneWayFromSourceRequired|Pk|elementLabelId|SqlDbType.Int|False|
|UId|ELEMENT_LABEL_UID|TwoWayRequired|UId|elementLabelUId|SqlDbType.UniqueIdentifier|False|
|Name|LABEL_NAME|TwoWayRequired|Name|elementLabelName|-|False|
|Description|LABEL_DESC|TwoWayRequired|Unknown|elementLabelDesc|-|False|
|IsActive|ISACTIVE|TwoWayRequired|Unknown|isActive|-|False|
|IsBuiltIn|ISBUILTIN|TwoWayRequired|Unknown|isBuiltIn|-|False|
|IsGlobalEdit|ISGLOBALEDIT|TwoWayRequired|Unknown|isGlobalEdit|-|False|
|IsPrivate|ISPRIVATE|TwoWayRequired|Unknown|isPrivate|-|False|
|DomainUId|DOMAIN_UID|TwoWayRequired|Unknown|domainUId|-|False|
|DomainName|DOMAIN_NAME|OneWayFromSourceRequired|Unknown|domainName|-|False|
|IsDomainActive|ISACTIVE_DOMAIN|OneWayFromSourceRequired|Unknown|isDomainActive|-|False|
|RlsOwner|RLS_OWNER|TwoWayRequired|Unknown|rlsOwner|-|True|
|RlsMask|RLS_MASK|TwoWayRequired|Unknown|rlsMask|-|True|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
