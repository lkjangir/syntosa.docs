# Syntosa.Core.ObjectModel.TypeItem
** need class/table description **
DataTable: TYPE

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|The primary key of this TypeItem.|Int32|True|True|
|UId|The unique identifier of this TypeItem.|Guid|True|True|
|Name|The name of this TypeItem.|String|True|True|
|Description|The friendly description of this TypeItem.|String|True|True|
|IsActive|Indicates 'deleted' status, where IsActive=false is excluded from normal query operations.|Boolean|True|True|
|IsBuiltIn|Indicates items which are integral to system function.  IsBuiltIn=true items cannot be deleted.|Boolean|True|True|
|IsAssignable|Indicates if this Type can be assigned.  IsAssignable=false will trigger a validation error on a foreign-key reference.|Boolean|True|True|
|IsNotifiable|Indicates if this Type can receive notification, such as in the case of a human or queue.|Boolean|True|True|
|IsAutoCollect|Indicates if element data is collected though manual input (AutoCollect=false) or automated processes (AutoCollect=true).|Boolean|True|True|
|ModuleUIdAutoCollect|Foreign key to Module table.  Indicates 'owner' of AutoCollect data; default to Syntosa if IsAutoCollect=false to maintain queryability of data.|Guid|True|True|
|SortOrder|Used to manually specify item's position in a list; overrides default alpha sort order.|Int32|True|True|
|TypeFunctionUId|Foreign key to TypeFunction table; the TypeFunction for this TypeItem.|Guid|True|True|
|TypeUnitUId|Foreign key to TypeUnit table; the TypeUnit for this TypeItem.|Guid|True|True|
|UiExtensions|An Xml blob of UI-specific settings, such as IsExpanded, IsDirty, etc.|String|True|True|
|ParentUId|Self-referencing foreign key to the parent TypeItem for this TypeItem.|Guid|True|True|
|ParentName|** need property description **|String|True|False|
|IsParentActive|** need property description **|Boolean|True|False|
|Children|A list of the child TypeItems of this TypeItem.|List`1|True|True|
|Syntosa.Core.ObjectModel.ISyntosaHierRecord.Children|** need property description **|IList|True|False|
|TypeFunctionModuleUId|** need property description **|Guid|True|False|
|TypeFunctionName|** need property description **|String|True|False|
|IsTypeFunctionActive|** need property description **|Boolean|True|False|
|TypeUnitName|** need property description **|String|True|False|
|IsTypeUnitActive|** need property description **|Boolean|True|False|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|ModifiedBy|The last user to modify the item.|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|TYPE_PK|OneWayFromSourceRequired|Pk|typeItemId|SqlDbType.Int|False|
|UId|TYPE_UID|TwoWayRequired|UId|typeItemUId|SqlDbType.UniqueIdentifier|False|
|Name|TYPE_NAME|TwoWayRequired|Name|typeItemName|-|False|
|Description|TYPE_DESC|TwoWayRequired|Unknown|typeItemDesc|-|False|
|IsActive|ISACTIVE|TwoWayOptional|Unknown|isActive|-|False|
|IsBuiltIn|ISBUILTIN|TwoWayOptional|Unknown|isBuiltIn|-|False|
|IsAssignable|ISASSIGNABLE|TwoWayRequired|Unknown|isAssignable|-|False|
|IsNotifiable|ISNOTIFIABLE|TwoWayRequired|Unknown|isNotifiable|-|False|
|IsAutoCollect|ISAUTOCOLLECT|TwoWayRequired|Unknown|isAutoCollect|-|False|
|ModuleUIdAutoCollect|MODULE_UID_AUTOCOLLECT|TwoWayRequired|Unknown|moduleUIdAutoCollect|-|False|
|SortOrder|UISORT_ORDER|TwoWayRequired|Unknown|sortOrder|-|False|
|TypeFunctionUId|TYPE_FUNCTION_UID|TwoWayRequired|Unknown|typeFunctionUId|-|False|
|TypeUnitUId|TYPE_UNIT_UID|TwoWayRequired|Unknown|typeUnitUId|-|False|
|UiExtensions|UI_EXTENSIONS|TwoWayOptional|Unknown|uiExtensions|-|True|
|ParentUId|TYPE_UID_PARENT|TwoWayRequired|ParentUId|parentUId|-|True|
|ParentName|TYPE_NAME_PARENT|OneWayFromSourceRequired|Unknown|parentName|-|False|
|IsParentActive|ISACTIVE_TYPE_PARENT|OneWayFromSourceRequired|Unknown|isParentActive|-|False|
|Children|None|None|Unknown|-|-|N/A|
|Syntosa.Core.ObjectModel.ISyntosaHierRecord.Children|None|None|Unknown|-|-|N/A|
|TypeFunctionModuleUId|TYPE_FUNCTION_MODULE_UID|OneWayFromSourceRequired|Unknown|typeFunctionModuleUId|-|False|
|TypeFunctionName|TYPE_FUNCTION_NAME|OneWayFromSourceRequired|Unknown|typeFunctionName|-|False|
|IsTypeFunctionActive|ISACTIVE_TYPE_FUNCTION|OneWayFromSourceRequired|Unknown|isTypeFunctionActive|-|False|
|TypeUnitName|TYPE_UNIT_NAME|OneWayFromSourceRequired|Unknown|typeUnitName|-|False|
|IsTypeUnitActive|ISACTIVE_TYPE_UNIT|OneWayFromSourceRequired|Unknown|isTypeUnitActive|-|False|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
