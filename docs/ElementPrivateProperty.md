# Syntosa.Core.ObjectModel.ElementPrivateProperty
** need class/table description **
DataTable: ELEMENT_PRIVATEPROPERTY

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|The primary key of this PrivateProperty.|Int32|True|True|
|UId|The unique identifier of this PrivateProperty.|Guid|True|True|
|PrivatePropertyKeyUId||Guid|True|True|
|SortOrder|Used to manually specify item's position in a list; overrides default alpha sort order.|Int32|True|True|
|Name|Alias for TypeName field; gets/sets TypeName.|String|True|True|
|TypeKeyUId|** need property description **|Guid|True|False|
|TypeKeyName|** need property description **|String|True|False|
|IsTypeKeyActive|** need property description **|Boolean|True|False|
|TypeValueUId|** need property description **|Guid|True|False|
|TypeValueName|** need property description **|String|True|False|
|IsTypeValueActive|** need property description **|Boolean|True|False|
|ElementUId|** need property description **|Guid|True|False|
|ElementName|** need property description **|String|True|False|
|IsElementActive|** need property description **|Boolean|True|False|
|Attribute|The value at the relationship point.|String|True|True|
|IsActive|Indicates 'deleted' status, where IsActive=false is excluded from normal query operations.|Boolean|True|True|
|ParentUId|Self-referencing foreign key to the parent Element for this Element.|Guid|True|True|
|Children|A list of the child Elements of this Element.|List`1|True|True|
|Syntosa.Core.ObjectModel.ISyntosaHierRecord.Children|** need property description **|IList|True|False|
|RlsOwner|The row-level security Owner UId.|Guid|True|True|
|RlsMask|The row-level security bitmask for this Element.|Byte[]|True|True|
|PrivatePropertyEdges|** need property description **|List`1|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|ModifiedBy|The last user to modify the item.|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|ELEMENT_PRIVATEPROPERTY_PK|OneWayFromSourceRequired|Pk|privatePropertyId|SqlDbType.Int|False|
|UId|ELEMENT_PRIVATEPROPERTY_UID|TwoWayRequired|UId|privatePropertyUId|SqlDbType.UniqueIdentifier|False|
|PrivatePropertyKeyUId|ELEMENT_PRIVATEPROPERTY_KEY_UID|TwoWayRequired|Unknown|privatePropertyKeyUId|-|False|
|SortOrder|UISORT_ORDER|TwoWayRequired|Unknown|sortOrder|-|False|
|Name|-|None|Unknown|name|-|N/A|
|TypeKeyUId|TYPE_UID_KEY|OneWayFromSourceRequired|Unknown|typeKeyUId|-|False|
|TypeKeyName|TYPE_UID_NAME_KEY|OneWayFromSourceRequired|Name|typeKeyName|-|False|
|IsTypeKeyActive|ISACTIVE_TYPE_KEY|OneWayFromSourceRequired|Unknown|isTypeKeyActive|-|False|
|TypeValueUId|TYPE_UID_VALUE|OneWayFromSourceRequired|Unknown|typeValueUId|-|False|
|TypeValueName|TYPE_UID_NAME_VALUE|OneWayFromSourceRequired|Unknown|typeValueName|-|False|
|IsTypeValueActive|ISACTIVE_TYPE_VALUE|OneWayFromSourceRequired|Unknown|isTypeValueActive|-|False|
|ElementUId|ELEMENT_UID|OneWayFromSourceRequired|Unknown|elementUId|-|False|
|ElementName|ELEMENT_NAME|OneWayFromSourceRequired|Unknown|elementName|-|False|
|IsElementActive|ISACTIVE_ELEMENT|OneWayFromSourceRequired|Unknown|isElementActive|-|False|
|Attribute|PRIVATE_ATTRIBUTE|TwoWayRequired|Unknown|attribute|-|False|
|IsActive|ISACTIVE|TwoWayOptional|Unknown|isActive|-|False|
|ParentUId|ELEMENT_PRIVATEPROPERTY_UID_PARENT|TwoWayRequired|ParentUId|parentUId|-|True|
|Children|None|None|Unknown|-|-|N/A|
|Syntosa.Core.ObjectModel.ISyntosaHierRecord.Children|None|None|Unknown|-|-|N/A|
|RlsOwner|RLS_OWNER|TwoWayRequired|Unknown|rlsOwner|-|True|
|RlsMask|RLS_MASK|TwoWayRequired|Unknown|rlsMask|-|True|
|PrivatePropertyEdges|None|None|Unknown|-|-|N/A|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
