# Syntosa.Core.ObjectModel.EdgeGlobalPropertyPrivatePropertyProperty
** need class/table description **
DataTable: EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|** need property description **|Int32|True|True|
|UId|** need property description **|Guid|True|True|
|EdgeType|** need property description **|SyntosaRecordType|True|False|
|EdgeUId|** need property description **|Guid|True|True|
|IsActiveTypeEdge|** need property description **|Boolean|True|False|
|ElementGlobalpropertyUId|** need property description **|Guid|True|False|
|ElementPrivatePropertyUId|** need property description **|Guid|True|False|
|ElementGlobalpropertyUIdDriver|** need property description **|Boolean|True|False|
|TypeNameEdge|** need property description **|String|True|False|
|TypeUIdGlobalproperty|** need property description **|Guid|True|False|
|GlobalAttribute|** need property description **|String|True|False|
|ElementPrivatePropertyKeyUId|** need property description **|Guid|True|False|
|PrivateAttribute|** need property description **|String|True|False|
|TypeNameGlobalproperty|** need property description **|String|True|False|
|IsActiveTypeGlobalproperty|** need property description **|Boolean|True|False|
|TypeUIdKeyPrivateProperty|** need property description **|Guid|True|False|
|TypeUIdValuePrivateProperty|** need property description **|Guid|True|False|
|TypeUnitUIdPrivateProperty|** need property description **|Guid|True|False|
|TypeNameKeyPrivateProperty|** need property description **|String|True|False|
|IsActiveKeyPrivateProperty|** need property description **|Boolean|True|False|
|TypeNameValuePrivateProperty|** need property description **|String|True|False|
|IsActiveValuePrivateProperty|** need property description **|Boolean|True|False|
|TypeUnitNamePrivateProperty|** need property description **|String|True|False|
|IsActiveTypeUnitPrivateProperty|** need property description **|Boolean|True|False|
|ElementNameGlobalproperty|** need property description **|String|True|False|
|IsActiveElementGlobalproperty|** need property description **|Boolean|True|False|
|TypeUIdElementGlobalproperty|** need property description **|Guid|True|False|
|ElementNamePrivateProperty|** need property description **|String|True|False|
|IsActiveElementPrivateProperty|** need property description **|Boolean|True|False|
|TypeUIdElementPrivateProperty|** need property description **|Guid|True|False|
|Name|** need property description **|String|True|True|
|ModifiedBy|The last user to modify the item.|String|True|True|
|TypeItemUId|** need property description **|Guid|True|True|
|TypeItemName|** need property description **|String|True|True|
|Attribute|** need property description **|String|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY_PK|OneWayFromSourceRequired|Pk|edgeGlobalPrivatePropId|SqlDbType.Int|False|
|UId|EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PROPERTY_UID|TwoWayRequired|UId|edgeGlobalPrivatePropUId|SqlDbType.UniqueIdentifier|False|
|EdgeType|None|None|Unknown|-|-|N/A|
|EdgeUId|EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_UID|TwoWayRequired|Unknown|edgeUId|-|False|
|IsActiveTypeEdge|ISACTIVE_TYPE_EDGE|OneWayFromSourceRequired|Unknown|isActiveTypeEdge|-|False|
|ElementGlobalpropertyUId|ELEMENT_GLOBALPROPERTY_UID|OneWayFromSourceRequired|Unknown|elementGlobalpropertyUId|-|False|
|ElementPrivatePropertyUId|ELEMENT_PRIVATEPROPERTY_UID|OneWayFromSourceRequired|Unknown|elementPrivatePropertyUId|-|False|
|ElementGlobalpropertyUIdDriver|ELEMENT_GLOBALPROPERTY_UID_DRIVER|OneWayFromSourceRequired|Unknown|elementGlobalpropertyUIdDriver|-|False|
|TypeNameEdge|TYPE_NAME_EDGE|OneWayFromSourceRequired|Unknown|typeNameEdge|-|False|
|TypeUIdGlobalproperty|TYPE_UID_GLOBALPROPERTY|OneWayFromSourceRequired|Unknown|typeUIdGlobalproperty|-|False|
|GlobalAttribute|GLOBAL_ATTRIBUTE|OneWayFromSourceRequired|Unknown|globalAttribute|-|False|
|ElementPrivatePropertyKeyUId|ELEMENT_PRIVATEPROPERTY_KEY_UID|OneWayFromSourceRequired|Unknown|elementPrivatePropertyKeyUId|-|False|
|PrivateAttribute|PRIVATE_ATTRIBUTE|OneWayFromSourceRequired|Unknown|privateAttribute|-|False|
|TypeNameGlobalproperty|TYPE_NAME_GLOBALPROPERTY|OneWayFromSourceRequired|Unknown|typeNameGlobalproperty|-|False|
|IsActiveTypeGlobalproperty|ISACTIVE_TYPE_GLOBALPROPERTY|OneWayFromSourceRequired|Unknown|isActiveTypeGlobalproperty|-|False|
|TypeUIdKeyPrivateProperty|TYPE_UID_KEY_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|typeUIdKeyPrivateProperty|-|False|
|TypeUIdValuePrivateProperty|TYPE_UID_VALUE_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|typeUIdValuePrivateProperty|-|False|
|TypeUnitUIdPrivateProperty|TYPE_UNIT_UID_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|typeUnitUIdPrivateProperty|-|False|
|TypeNameKeyPrivateProperty|TYPE_NAME_KEY_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|typeNameKeyPrivateProperty|-|False|
|IsActiveKeyPrivateProperty|ISACTIVE_KEY_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|isActiveKeyPrivateProperty|-|False|
|TypeNameValuePrivateProperty|TYPE_NAME_VALUE_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|typeNameValuePrivateProperty|-|False|
|IsActiveValuePrivateProperty|ISACTIVE_VALUE_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|isActiveValuePrivateProperty|-|False|
|TypeUnitNamePrivateProperty|TYPE_UNIT_NAME_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|typeUnitNamePrivateProperty|-|False|
|IsActiveTypeUnitPrivateProperty|ISACTIVE_TYPE_UNIT_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|isActiveTypeUnitPrivateProperty|-|False|
|ElementNameGlobalproperty|ELEMENT_NAME_GLOBALPROPERTY|OneWayFromSourceRequired|Unknown|elementNameGlobalproperty|-|False|
|IsActiveElementGlobalproperty|ISACTIVE_ELEMENT_GLOBALPROPERTY|OneWayFromSourceRequired|Unknown|isActiveElementGlobalproperty|-|False|
|TypeUIdElementGlobalproperty|TYPE_UID_ELEMENT_GLOBALPROPERTY|OneWayFromSourceRequired|Unknown|typeUIdElementGlobalproperty|-|False|
|ElementNamePrivateProperty|ELEMENT_NAME_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|elementNamePrivateProperty|-|False|
|IsActiveElementPrivateProperty|ISACTIVE_ELEMENT_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|isActiveElementPrivateProperty|-|False|
|TypeUIdElementPrivateProperty|TYPE_UID_ELEMENT_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|typeUIdElementPrivateProperty|-|False|
|Name|-|None|Unknown|name|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|TypeItemUId|TYPE_UID_EDGE_PROPERTY|TwoWayRequired|Unknown|typeItemUId|-|False|
|TypeItemName|TYPE_NAME_EDGE_PROPERTY|OneWayFromSourceRequired|Name|typeItemName|-|False|
|Attribute|EDGE_PROPERTY_ATTRIBUTE|TwoWayRequired|Unknown|attribute|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
