# Syntosa.Core.ObjectModel.EdgeGlobalPropertyPrivateProperty
** need class/table description **
DataTable: EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF

|Name|Description|PropertyType|CanRead|CanWrite|
|-------|-------|-------|-------|-------|
|Id|** need property description **|Int32|True|True|
|UId|** need property description **|Guid|True|True|
|LeftRecordType|** need property description **|SyntosaRecordType|True|False|
|LeftUId|** need property description **|Guid|True|True|
|GlobalPropertyUId|** need property description **|Guid|True|True|
|GlobalPropertyTypeUId|** need property description **|Guid|True|False|
|GlobalPropertyTypeName|** need property description **|String|True|False|
|IsGlobalPropertyTypeActive|** need property description **|Boolean|True|False|
|GlobalAttribute|** need property description **|String|True|False|
|RightRecordType|** need property description **|SyntosaRecordType|True|False|
|RightUId|** need property description **|Guid|True|True|
|PrivatePropertyUId|** need property description **|Guid|True|True|
|PrivatePropertyKeyUId|** need property description **|Guid|True|False|
|PrivatePropertyTypeKeyUId|** need property description **|Guid|True|False|
|PrivatePropertyTypeValueUId|** need property description **|Guid|True|False|
|PrivatePropertyTypeNameKey|** need property description **|String|True|False|
|IsPrivatePropertyKeyActive|** need property description **|Boolean|True|False|
|PrivatePropertyTypeNameValue|** need property description **|String|True|False|
|IsPrivatePropertyValueActive|** need property description **|Boolean|True|False|
|PrivatePropertyTypeUnitUId|** need property description **|Guid|True|False|
|PrivatePropertyTypeUnitName|** need property description **|String|True|False|
|PrivatePropertyIsTypeUnitActive|** need property description **|Boolean|True|False|
|PrivateAttribute|** need property description **|String|True|False|
|PrivatePropertyElementName|** need property description **|String|True|False|
|IsPrivatePropertyElementActive|** need property description **|Boolean|True|False|
|PrivatePropertyElementTypeUId|** need property description **|Guid|True|False|
|IsLeftUIdSource|** need property description **|Boolean|True|True|
|IsGlobalPropertyDriver|** need property description **|Boolean|True|True|
|EdgeProperties|** need property description **|List`1|True|True|
|Syntosa.Core.ObjectModel.ISyntosaEdgeProperty.EdgeProperties|** need property description **|IList|True|False|
|ElementName|** need property description **|String|True|False|
|IsElementActive|** need property description **|Boolean|True|False|
|ElementTypeUId|** need property description **|Guid|True|False|
|Name|** need property description **|String|True|True|
|ModifiedBy|The last user to modify the item.|String|True|True|
|TypeItemUId|** need property description **|Guid|True|True|
|TypeItemName|** need property description **|String|True|True|
|IsTypeItemActive|** need property description **|Boolean|True|True|
|SysStartTime|Reflects system edit start time.|DateTime|True|True|
|SysEndTime|Reflects system edit end time.|DateTime|True|True|
|IsNew|Indicates if the record already exists in the database. IsNew == true: the record does not exist.|Boolean|True|False|
|InitialHashCode|The hash of the record when it was created/selected.|Int32|True|True|
|CurrentHashCode|The current hash of the record.  CurrentHashCode != InitialHashCode: IsDirty = true.|Int32|True|False|
|IsDirty|Compares CurrentHashCode to InitialHashCode to determine if the record has beem modified.  CurrentHashCode != InitialHashCode: IsDirty = true.|Boolean|True|False|
|IsDeleted|Indicates if the record is marked for deletion.  IsDeleted == true: the record will be deleted from the database during Upsert actions.|Boolean|True|True|


|Name|DataField|Binding|FieldType|ParameterName|SqlDbType|SqlDbNullable|
|-------|-------|-------|-------|-------|-------|-------|
|Id|EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_PK|OneWayFromSourceRequired|Pk|edgeGlobalPrivateId|SqlDbType.Int|False|
|UId|EDGE_ELEMENT_GLOBALPROPERTY_PRIVATEPROPERTY_XREF_UID|TwoWayRequired|UId|edgeGlobalPrivateUId|SqlDbType.UniqueIdentifier|False|
|LeftRecordType|None|None|Unknown|-|-|N/A|
|LeftUId|None|None|Unknown|-|-|N/A|
|GlobalPropertyUId|ELEMENT_GLOBALPROPERTY_UID|TwoWayRequired|Unknown|globalPropertyUId|-|False|
|GlobalPropertyTypeUId|TYPE_UID_GLOBALPROPERTY|OneWayFromSourceRequired|Unknown|globalPropertyTypeUId|-|False|
|GlobalPropertyTypeName|TYPE_NAME_GLOBALPROPERTY|OneWayFromSourceRequired|Unknown|globalPropertyTypeName|-|False|
|IsGlobalPropertyTypeActive|ISACTIVE_TYPE_GLOBALPROPERTY|OneWayFromSourceRequired|Unknown|isGlobalPropertyTypeActive|-|False|
|GlobalAttribute|GLOBAL_ATTRIBUTE|OneWayFromSourceRequired|Unknown|globalAttribute|-|False|
|RightRecordType|None|None|Unknown|-|-|N/A|
|RightUId|None|None|Unknown|-|-|N/A|
|PrivatePropertyUId|ELEMENT_PRIVATEPROPERTY_UID|TwoWayRequired|Unknown|privatePropertyUId|-|False|
|PrivatePropertyKeyUId|ELEMENT_PRIVATEPROPERTY_KEY_UID|OneWayFromSourceRequired|Unknown|privatePropertyKeyUId|-|False|
|PrivatePropertyTypeKeyUId|TYPE_UID_KEY_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|privatePropertyTypeKeyUId|-|False|
|PrivatePropertyTypeValueUId|TYPE_UID_VALUE_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|privatePropertyTypeValueUId|-|False|
|PrivatePropertyTypeNameKey|TYPE_NAME_KEY_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|privatePropertyTypeNameKey|-|False|
|IsPrivatePropertyKeyActive|ISACTIVE_KEY_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|isPrivatePropertyKeyActive|-|False|
|PrivatePropertyTypeNameValue|TYPE_NAME_VALUE_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|privatePropertyTypeNameValue|-|False|
|IsPrivatePropertyValueActive|ISACTIVE_VALUE_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|isPrivatePropertyValueActive|-|False|
|PrivatePropertyTypeUnitUId|TYPE_UNIT_UID_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|privatePropertyTypeUnitUId|-|False|
|PrivatePropertyTypeUnitName|TYPE_UNIT_NAME_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|privatePropertyTypeUnitName|-|False|
|PrivatePropertyIsTypeUnitActive|ISACTIVE_TYPE_UNIT_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|privatePropertyIsTypeUnitActive|-|False|
|PrivateAttribute|PRIVATE_ATTRIBUTE|OneWayFromSourceRequired|Unknown|privateAttribute|-|False|
|PrivatePropertyElementName|ELEMENT_NAME_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|privatePropertyElementName|-|False|
|IsPrivatePropertyElementActive|ISACTIVE_ELEMENT_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|isPrivatePropertyElementActive|-|False|
|PrivatePropertyElementTypeUId|TYPE_UID_ELEMENT_PRIVATEPROPERTY|OneWayFromSourceRequired|Unknown|privatePropertyElementTypeUId|-|False|
|IsLeftUIdSource|None|None|Unknown|-|-|N/A|
|IsGlobalPropertyDriver|ELEMENT_GLOBALPROPERTY_UID_DRIVER|TwoWayRequired|Unknown|isGlobalPropertyDriver|-|False|
|EdgeProperties|None|None|Unknown|-|-|N/A|
|Syntosa.Core.ObjectModel.ISyntosaEdgeProperty.EdgeProperties|None|None|Unknown|-|-|N/A|
|ElementName|ELEMENT_NAME|OneWayFromSourceRequired|Unknown|elementName|-|False|
|IsElementActive|ISACTIVE_ELEMENT|OneWayFromSourceRequired|Unknown|isElementActive|-|False|
|ElementTypeUId|TYPE_UID_ELEMENT|OneWayFromSourceRequired|Unknown|elementTypeUId|-|False|
|Name|-|None|Unknown|name|-|N/A|
|ModifiedBy|MODIFIED_BY|TwoWayRequired|Unknown|modifiedBy|-|False|
|TypeItemUId|TYPE_UID_EDGE|TwoWayRequired|Unknown|typeItemUId|-|False|
|TypeItemName|TYPE_NAME_EDGE|OneWayFromSourceRequired|Name|typeItemName|-|False|
|IsTypeItemActive|ISACTIVE_TYPE_EDGE|OneWayFromSourceRequired|Unknown|isTypeItemActive|-|False|
|SysStartTime|SYS_START_TIME|OneWayFromSourceRequired|Unknown|sysStartTime|-|False|
|SysEndTime|SYS_END_TIME|OneWayFromSourceRequired|Unknown|sysEndTime|-|False|
|IsNew|None|None|Unknown|-|-|N/A|
|InitialHashCode|None|None|Unknown|-|-|N/A|
|CurrentHashCode|None|None|Unknown|-|-|N/A|
|IsDirty|None|None|Unknown|-|-|N/A|
|IsDeleted|None|None|Unknown|-|-|N/A|
